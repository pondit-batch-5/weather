import 'dart:async';

void main() {
  task1();
  task2();
  task3('');


}

void task1(){
  print ('Task one');
}

void task2() async{
  await Future.delayed(Duration(seconds: 10));
  print ('Task two');
  // task3('Task two');
}

void task3(String task2){
  print ('Task three $task2');
}
