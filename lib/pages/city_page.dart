import 'package:flutter/material.dart';

class LocationByCityPage extends StatefulWidget {
  const LocationByCityPage({super.key});

  @override
  State<LocationByCityPage> createState() => _LocationByCityPageState();
}

class _LocationByCityPageState extends State<LocationByCityPage> {
  TextEditingController cityTxtController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('City'),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: cityTxtController,
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search),
                  hintText: 'Enter your city',
                  fillColor: Colors.grey,
                  filled: true,
                  border: InputBorder.none,
                ),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            ElevatedButton(
              onPressed: () {
                if (cityTxtController.text == '') {
                  print('Please enter city');
                } else {
                  Navigator.pop(context, cityTxtController.text);
                }
              },
              child: Text('Search Weather'),
            )
          ],
        ),
      ),
    );
  }
}
