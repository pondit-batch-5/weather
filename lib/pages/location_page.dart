import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart';
import 'package:my_location/models/weather_model.dart';
import 'package:my_location/pages/city_page.dart';
import 'package:my_location/viewmodel/weather_viewmodel.dart';

class LocationPage extends StatefulWidget {
  const LocationPage({super.key});

  @override
  State<LocationPage> createState() => _LocationPageState();
}

class _LocationPageState extends State<LocationPage> {
  bool isLoading = false;
  WeatherModel? model;
  final weatherViewModel = WeatherViewModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextButton(
                      onPressed: () async {
                        isLoading = true;
                        setState(() {});
                        bool isPermitted =
                            await weatherViewModel.getRequestPermission();
                        if (isPermitted == true) {
                          model =
                              await weatherViewModel.getMyLocationFormDevice();
                        }
                        isLoading = false;
                        setState(() {});
                      },
                      child: Icon(
                        Icons.near_me,
                        color: Colors.white,
                      ),
                    ),
                    TextButton(
                      onPressed: () async {
                        String? cityName = await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) {
                              return LocationByCityPage();
                            },
                          ),
                        );
                        if (cityName != null) {
                          isLoading = true;
                          setState(() {});
                          model = await weatherViewModel
                              .currentWeatherByCityName(cityName);
                          isLoading = false;
                          setState(() {});
                        }
                      },
                      child: Icon(
                        Icons.location_city,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 16.0),
                  child: Text(
                    '${model?.main?.temp!.toStringAsFixed(2)} C',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16, bottom: 32),
                  child: Row(
                    children: [
                      Text(
                        'Description: ',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Text(
                        model?.weather![0].description ?? '',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16, bottom: 32),
                  child: Row(
                    children: [
                      Text(
                        'City Name:',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Text(
                        model?.name ?? '',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
    ));
  }
}
